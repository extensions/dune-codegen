
#include "config.h"
#include "dune/common/parallel/mpihelper.hh"
#include "dune/grid/yaspgrid.hh"
#include "dune/testtools/gridconstruction.hh"
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"
#include <random>
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "string"
#include "dune/codegen/vtkpredicate.hh"
#include "dune/pdelab/gridfunctionspace/gridfunctionadapter.hh"
#include "dune/pdelab/common/functionutilities.hh"

#include "dune/pdelab.hh"
#include "poisson_dg_2d_generated_matrix_free_preconditioner_poisson_matrix_free_driverblock.hh"
#include "poisson_dg_2d_generated_matrix_free_preconditioner_poisson_matrix_based_driverblock.hh"



int main(int argc, char** argv){  
  try
  {    

    if (argc != 2){
      std::cerr << "This program needs to be called with an ini file" << std::endl;
      return 1;
    }
    
    // Initialize basic stuff...    
    Dune::MPIHelper& mpihelper = Dune::MPIHelper::instance(argc, argv);
    using RangeType = double;
    Dune::ParameterTree initree;
    Dune::ParameterTreeParser::readINITree(argv[1], initree);
    
    // Setup grid (view)...    
    using Grid = Dune::YaspGrid<2, Dune::EquidistantCoordinates<RangeType, 2>>;
    using GV = Grid::LeafGridView;
    IniGridFactory<Grid> factory(initree);
    std::shared_ptr<Grid> grid = factory.getGrid();
    GV gv = grid->leafGridView();
    
    // Set up driver block...    
    DriverBlockPoissonMatrixFree<GV> driverBlockPoisson_matrix_free(gv, initree);
    auto solver_matrix_free = driverBlockPoisson_matrix_free.getSolver();
    solver_matrix_free->apply();
    auto result_matrix_free = solver_matrix_free->result();
    std::cout << "Number of linear solver iteartions: " << result_matrix_free.linear_solver_iterations << std::endl;

    // Set up driver block...
    DriverBlockPoissonMatrixBased<GV> driverBlockPoisson_matrix_based(gv, initree);
    auto solver_matrix_based = driverBlockPoisson_matrix_based.getSolver();
    solver_matrix_based->apply();
    auto result_matrix_based = solver_matrix_based->result();
    std::cout << "Number of linear solver iteartions: " << result_matrix_based.linear_solver_iterations << std::endl;

    bool testfail(false);
    // if (result_matrix_free.linear_solver_iterations != result_matrix_based.linear_solver_iterations)
    //   testfail = true;
    return testfail;
  }
  catch (Dune::Exception& e)
  {    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }  
  catch (std::exception& e)
  {    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }  
}

