add_subdirectory(backend)
add_subdirectory(gridfunctionspace)
add_subdirectory(gridoperator)
add_subdirectory(preconditioner)

install(FILES blockstructuredqkfem.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/codegen/blockstructured
        )
