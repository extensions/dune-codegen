// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CODEGEN_BLOCKSTRUCTURED_GRIDFUNCTIONSPACE_VISITORS_HH
#define DUNE_CODEGEN_BLOCKSTRUCTURED_GRIDFUNCTIONSPACE_VISITORS_HH

#include <dune/typetree/visitor.hh>

namespace Dune{
  namespace PDELab {
    namespace Blockstructured {

      // copy parts of Dune::PDELab::ComputeSizeVisitor to circumvent warning due to deriving from anonymous namespace
      template<typename = int>
      struct PropagateGlobalStorageVisitor
        : public TypeTree::TreeVisitor
        , public TypeTree::DynamicTraversal {

        template<typename LFS, typename Child, typename TreePath, typename ChildIndex>
        void beforeChild(const LFS &lfs, Child &child, TreePath treePath, ChildIndex childIndex) const {
          child._dof_indices = lfs._dof_indices;
          child.subentityWiseDOFs_ptr = lfs.subentityWiseDOFs_ptr;
        }
      };

      // copy parts of Dune::PDELab::ComputeSizeVisitor to circumvent warning due to deriving from anonymous namespace
      template<typename Entity>
      struct ComputeSizeVisitor
        : public TypeTree::TreeVisitor
        , public TypeTree::DynamicTraversal {

        template<typename Node, typename TreePath>
        void pre(Node &node, TreePath treePath) {
          node.offset = offset;
          node.offsetLeafs = leafOffset;
        }

        template<typename Node, typename TreePath>
        void post(Node &node, TreePath treePath) {
          node.n = offset - node.offset;
          node.nLeafs = leafOffset - node.offsetLeafs;
        }

        template<typename Node, typename TreePath>
        void leaf(Node &node, TreePath treePath) {
          node.offset = offset;
          Node::FESwitch::setStore(node.pfe, node.pgfs->finiteElementMap().find(e));
          node.n = Node::FESwitch::basis(*node.pfe).size();
          offset += node.n;

          node.offsetLeafs = leafOffset;
          node.nLeafs = 1;
          leafOffset++;
        }

        ComputeSizeVisitor(const Entity& entity, std::size_t offset_ = 0)
          : e(entity)
          , offset(offset_)
          , leafOffset(0)
        {}

        const Entity& e;
        std::size_t offset;
        std::size_t leafOffset;
      };


      // copy parts of Dune::PDELab::FillIndicesVisitor to circumvent warning due to deriving from anonymous namespace
      template<typename Entity>
      struct FillIndicesVisitor
        : public TypeTree::TreeVisitor
        , public TypeTree::DynamicTraversal {

        template<typename Node, typename TreePath>
        void leaf(Node& node, TreePath treePath)
        {
          // setup DOFIndices for this finite element
          node.dofIndices(e,node._dof_indices->begin()+node.offset,node._dof_indices->begin()+node.offset+node.n,std::integral_constant<bool,false>{});
        }

        template<typename Node, typename Child, typename TreePath, typename ChildIndex>
        void afterChild(const Node &node, const Child &child, TreePath treePath, ChildIndex childIndex) {
          for (std::size_t i = 0; i < child.nLeafs; ++i)
            for (auto &index: (*node.subentityWiseDOFs_ptr)[child.offsetLeafs + i])
                index.treeIndex().push_back(childIndex);
        }

        FillIndicesVisitor(const Entity& entity)
          : e(entity)
        {}

        const Entity& e;
      };

    }
  }
}

#endif //DUNE_CODEGEN_BLOCKSTRUCTURED_GRIDFUNCTIONSPACE_VISITORS_HH
