#ifndef DUNE_CODEGEN_DEREFERENCE_POINTER_HH
#define DUNE_CODEGEN_DEREFERENCE_POINTER_HH

#include<memory>


/* Due to operator splitting we might encounter objects that might be available
 * directly or through a shared_ptr (eg the local function space). If we get a
 * pointer we want to dereference, otherwise we just want to work with the
 * object itself. The easiest way to get this behavior is through a small C++
 * function.
 */

template <typename T>
T& dereference(std::shared_ptr<T> x)
{
  return *x;
}

template <typename T>
T& dereference(T& x)
{
  return x;
}

#endif
