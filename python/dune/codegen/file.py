""" Manages the generation of C++ header files """

import subprocess

from cgen import Generable, Include

from dune.codegen.generation import retrieve_cache_items
from dune.codegen.options import get_option


def generate_file(filename, tag, content, headerguard=True):
    """Write a file from the generation cache.

    Arguments:
    ----------
    filename: str
        The filename to write the generated code to.
    tag: str
        The tag, that all entries related to this file in the cache have.
    content: list
        A list of Generables to put into the file.

    Keyword Arguments:
    ------------------
    headerguard: bool
        Whether a double inclusion protection header should be added to the file.
        The name of the macro is mangled from the absolute path. Defaults to True.
    """

    # Accumulate all code into single string
    code = ""

    # Add a double inclusion protection header
    if headerguard:
        macro = filename.upper().replace("/", "_").replace(".", "_").replace("-", "_")
        code += "#ifndef {0}\n#define {0}\n\n".format(macro)

    # Add pre include lines from the cache
    for define in retrieve_cache_items("{} and pre_include".format(tag)):
        code += "".join(define) + "\n"
    code += "\n"

    # Add the includes from the cache
    for inc in retrieve_cache_items("{} and include".format(tag)):
        assert isinstance(inc, Include)
        code += "".join(inc.generate()) + "\n"
    code += "\n"

    # Add post include lines direclty after includes
    for define in retrieve_cache_items("{} and post_include".format(tag)):
        code += "".join(define) + "\n"

    code += "\n\n"

    # Add main content
    for c in content:
        assert isinstance(c, Generable)
        code += "".join(c.generate()) + "\n\n"

    # Add end of file code
    for eof in retrieve_cache_items("{} and end_of_file".format(tag)):
        code += "".join(eof)

    # Close headerguard
    if headerguard:
        code += "\n\n#endif //{}\n".format(macro)

    # Run clang-format (installed via pip) inside project source dir
    code = subprocess.check_output(
        [
            "clang-format",
            "--style=file",
            "--fallback-style=Mozilla",
            "--sort-includes=False",
        ],
        text=True,
        input=code,
        cwd=get_option("project_srcdir"),
    )

    # Write the generated file
    with open(filename, "w") as file:
        file.write(code)
