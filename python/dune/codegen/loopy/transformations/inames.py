import loopy as lp
import pymbolic.primitives as prim
import islpy as isl
from dune.codegen.loopy.symbolic import substitute
from loopy.match import All
from loopy.symbolic import aff_to_expr
from loopy.isl_helpers import static_min_of_pw_aff


def remove_constant_inames(kernel):
    constant_inames = []
    for iname in kernel.all_inames():
        try:
            with isl.SuppressedWarnings(kernel.isl_context):
                if kernel.get_constant_iname_length(iname) == 1:
                    constant_inames.append((iname, aff_to_expr(static_min_of_pw_aff(
                        kernel.get_iname_bounds(iname).lower_bound_pw_aff, constants_only=True
                    ))))
        except isl.Error:
            pass

    if constant_inames:
        iname_set = frozenset([iname for iname, _ in constant_inames])
        repl_map = dict([(prim.Variable(iname), val) for iname, val in constant_inames])
        kernel = lp.map_instructions(kernel, All(),
                                     lambda insn: insn.with_transformed_expressions(
                                         lambda expr: substitute(expr, repl_map)))
        kernel = lp.map_instructions(kernel, All(),
                                     lambda insn: insn.copy(within_inames=insn.within_inames - iname_set))
        kernel = lp.remove_unused_inames(kernel, iname_set)
        kernel = kernel.copy(loop_priority=frozenset(tuple(i for i in p if i not in iname_set)
                                                     for p in kernel.loop_priority))

    return kernel
